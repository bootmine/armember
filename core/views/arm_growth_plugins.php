<div class="wrap arm_page arm_feature_settings_main_wrapper arm_groth_plugin_page">
	<div class="content_wrapper arm_feature_settings_content" id="content_wrapper">
		<div class="page_title"></div>
		<div class="armclear"></div>
		<div class="arm_feature_settings_wrapper">            
			<div class="arm_feature_settings_container arm-gt-mainwrapper">
	
				<div class="arm-mlc-head-wrap">
					<div class="arm-mlc-left-heading">
						<div class="arm-gt-page-inner-block-heading"> Our <span class="arm-page-heading-highlight"> Family WordPress Plugins </span>
						</div>
					</div>
				</div>

				<div class="arm-mlc-head-wrap">
					<div class="arm-mlc-left-heading">
						<div class="arm-gt-heading-main-inner-cls">
							<div class="arm-gt-page-inner-contain">
								You will get the same user-friendly experience throughout all of our plugins. Enjoy single-window 24/7
								support for all our plugins. All of our plugins are compatible with each other.
							</div>
						</div>
					</div>
				</div>
				<div class="arm-mlc-sec-space"></div>
				<div class="arm-mlc-head-wrap">
					<div class="arm-mlc-left-heading">
						<div class="arm-plugin-details-cls">
							<div class="arm-gt-plugin-icon">
								<span class="arm-plugin-icon arm-arm-icon"></span>
							</div>
							<div class="arm-gt-plugin-dec">
								<div class="arm-plugin-heading"> BookingPress <span class="arm-plugin-heading-cls"> - WordPress Booking Plugin</span></div>
								<div class="arm-plugin-heading-desc"> Imagine a WordPress BookingPress Plugin that's remarkably user-friendly, equipped with an extensive feature set, excelling in performance, and featuring a sleek modern interface. It distinguishes itself as a superior option, surpassing even the most popular Appointment Booking plugins available.</div>
								<div class="arm-plugin-key-feature"> Key Features: </div>
								<ul class="arm-feature-list-cls-plugin-dec">
									<li class="arm-feature-list-li-plugin"> Great UI And UX </li>
									<li class="arm-feature-list-li-plugin"> Interactive booking wizard </li>
									<li class="arm-feature-list-li-plugin"> Online Payment Gateways </li>
									<li class="arm-feature-list-li-plugin"> Offline Payment </li>
									<li class="arm-feature-list-li-plugin"> Built-in Spam Facility </li>
									<li class="arm-feature-list-li-plugin"> Custom Email Notifications </li>
								</ul>
								<div style="margin-top: 40px;" class="arm-gt-product-btn-seprator">
								<a href="https://wordpress.org/plugins/bookingpress-appointment-booking/" target="_blank" class="arm-learnmore-btn"> Learn More </a>
								<?php if ( (is_plugin_active('bookingpress-appointment-booking/bookingpress-appointment-booking.php')) || file_exists( WP_PLUGIN_DIR . '/bookingpress-appointment-booking/bookingpress-appointment-booking.php')  ) { ?>
										<button disabled="disabled" type="button" class="arm-btn arm-install-btn arm-plugin is-disabled">
											<span>
												<span class="arm-btn__label"> Installed </span>
												<div class="arm-btn--loader__circles">
													<div></div>
													<div></div>
													<div></div>
												</div>
											</span>
										</button>
									<?php } else { ?>	
										<button type="button" onclick="arm_download_plugins('bookingpress',this)" class="el-button arm-btn arm-install-btn arm-plugin el-button--default">
											<span>
												<span class="arm-btn__label"> Install </span>
												<div class="arm-btn--loader__circles">
													<div></div>
													<div></div>
													<div></div>
												</div>
											</span>
										</button>
									<?php }  ?>
								</div>
							</div>
							<hr class="arm-section-line">
						</div>
					</div>
				</div>

				

				<div class="arm-mlc-head-wrap">
					<div class="arm-mlc-left-heading">
						<div class="arm-plugin-details-cls">
							<div class="arm-gt-plugin-icon">
								<span class="arm-plugin-icon arm-arf-icon"></span>
							</div>
							<div class="arm-gt-plugin-dec">
								<div class="arm-plugin-heading arf-heading"> ARForms <span class="arm-plugin-heading-cls">- WordPress Form Builder Plugin </span></div>
								<div class="arm-plugin-heading-desc"> ARForms is an all-in-one WordPress form builder plugin. It not only allows you to create contact forms for your site but also empowers you to build WordPress forms such as feedback forms, survey forms, and various other types of forms with responsive designs. </div>
								<div class="arm-plugin-key-feature"> Key Features: </div>
								<ul class="arm-feature-list-cls-plugin-dec">
									<li class="arm-feature-list-li-plugin"> Real-Time Form Editor </li>
									<li class="arm-feature-list-li-plugin"> Multi-Column Option </li>
									<li class="arm-feature-list-li-plugin"> Styling &amp; Unlimited Color Option </li>
									<li class="arm-feature-list-li-plugin"> Built-In Anti Spam Protection </li>
									<li class="arm-feature-list-li-plugin"> Material &amp; Rounded Style Forms </li>
									<li class="arm-feature-list-li-plugin"> Popular Page Builders Support </li>
								</ul>
								<div style="margin-top: 40px;" class="arm-gt-product-btn-seprator">
									<a href="https://wordpress.org/plugins/arforms-form-builder/" target="_blank" class="arm-learnmore-btn arf-plugin"> Learn More </a>
									<?php if ( (is_plugin_active('arforms-form-builder/arforms-form-builder.php')) || file_exists( WP_PLUGIN_DIR . '/arforms-form-builder/arforms-form-builder.php') ) { ?>
										<button disabled="disabled" type="button" class="el-button el-button arm-btn arm-install-btn arf-plugin el-button--default is-disabled"><!----><!---->
											<span>
												<span class="arm-btn__label"> Installed </span>
												<div class="arm-btn--loader__circles">
													<div></div>
													<div></div>
													<div></div>
												</div>
											</span>
										</button>
									<?php } else { ?>
										<button type="button" onclick="arm_download_plugins('arforms',this)" class="el-button arm-btn arm-install-btn arf-plugin el-button--default"><!----><!---->
											<span>
												<span class="arm-btn__label"> Install </span>
												<div class="arm-btn--loader__circles">
													<div></div>
													<div></div>
													<div></div>
												</div>
											</span>
										</button>
									<?php }  ?>	
								</div>
							</div>
							<hr class="arm-section-line">
						</div>
					</div>
				</div>

				<div class="arm-mlc-head-wrap">
					<div class="arm-mlc-left-heading">
						<div class="arm-plugin-details-cls">
							<div class="arm-gt-plugin-icon">
								<span class="arm-plugin-icon arm-arp-icon"></span>
							</div>
							<div class="arm-gt-plugin-dec">
								<div class="arm-plugin-heading arp-heading"> ARPrice <span class="arm-plugin-heading-cls">- WordPress Pricing Table Plugin </span></div>
								<div class="arm-plugin-heading-desc">ARPrice is a WordPress pricing table plugin that enables you to effortlessly craft responsive pricing tables and plan comparison tables. With its powerful and flexible real-time editor, you can swiftly design pricing tables, using multiple templates, to suit various WordPress themes. </div>
								<div class="arm-plugin-key-feature"> Key Features: </div>
								<ul class="arm-feature-list-cls-plugin-dec">
									<li class="arm-feature-list-li-plugin"> Real-time Pricing Table Editor </li>
									<li class="arm-feature-list-li-plugin"> Unlimited Color Options </li>
									<li class="arm-feature-list-li-plugin"> Create Team Showcases </li>
									<li class="arm-feature-list-li-plugin"> Translation Ready </li>
									<li class="arm-feature-list-li-plugin"> Responsive Pricing Tables </li>
									<li class="arm-feature-list-li-plugin"> Multi-Site Compatible </li>
								</ul>
								<div style="margin-top: 40px;" class="arm-gt-product-btn-seprator">
									<a href="https://wordpress.org/plugins/arprice-responsive-pricing-table/" target="_blank" class="arm-learnmore-btn arp-plugin"> Learn More </a>
									<?php if ((is_plugin_active('arprice-responsive-pricing-table/arprice-responsive-pricing-table.php')) || file_exists( WP_PLUGIN_DIR . '/arprice-responsive-pricing-table/arprice-responsive-pricing-table.php')  ) { ?>
										<button disabled="disabled" type="button" class="el-button arm-btn arm-install-btn arp-plugin el-button--default is-disabled">
											<span>
												<span class="arm-btn__label"> Installed </span>
												<div class="arm-btn--loader__circles">
													<div></div>
													<div></div>
													<div></div>
												</div>
											</span>
										</button>
									<?php } else { ?>	
										<button type="button" onclick="arm_download_plugins('arprice',this)" class="el-button arm-btn arm-install-btn arp-plugin el-button--default ">
											<span>
												<span class="arm-btn__label"> Install </span>
												<div class="arm-btn--loader__circles">
													<div></div>
													<div></div>
													<div></div>
												</div>
											</span>
										</button>
									<?php }  ?>	
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="arm-mlc-sec-space"></div>
				<input type="hidden" name="arm_download_plugins_flag" id="arm_download_plugins_flag" value="0">

			</div>
            <?php $wpnonce = wp_create_nonce( 'arm_wp_nonce' );?>
			<input type="hidden" name="arm_wp_nonce" value="<?php echo esc_attr($wpnonce);?>"/>
		</div>
		<div class="armclear"></div>
	</div>
</div>
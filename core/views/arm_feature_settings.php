<?php 
global $wpdb, $ARMember, $arm_slugs, $arm_social_feature,$myplugarr, $arm_admin_mycred_feature;
$ARMember->arm_session_start();
$user_private_content = get_option('arm_is_user_private_content_feature');
$social_feature = get_option('arm_is_social_feature');
$social_login_feature = get_option('arm_is_social_login_feature');
$pro_ration_feature = get_option('arm_is_pro_ration_feature');
$drip_content_feature = get_option('arm_is_drip_content_feature');
//$opt_ins_feature = get_option('arm_is_opt_ins_feature');
$coupon_feature = get_option('arm_is_coupon_feature');
$buddypress_feature = get_option('arm_is_buddypress_feature');
$invoice_tax_feature = get_option('arm_is_invoice_tax_feature');
$multiple_membership_feature = get_option('arm_is_multiple_membership_feature');
$gutenberg_block_restriction_feature = get_option('arm_is_gutenberg_block_restriction_feature');
$arm_is_mycred_active = get_option('arm_is_mycred_feature');
$woocommerce_feature = get_option('arm_is_woocommerce_feature');
$arm_pay_per_post = get_option('arm_is_pay_per_post_feature');
$arm_api_service_feature = get_option('arm_is_api_service_feature');
$plan_limit_feature = get_option('arm_is_plan_limit_feature');
$beaver_builder_restriction_feature = get_option('arm_is_beaver_builder_restriction_feature');
$divi_builder_restriction_feature = get_option('arm_is_divi_builder_restriction_feature');
$wpbakery_page_builder_restriction_feature = get_option('arm_is_wpbakery_page_builder_restriction_feature');
$fusion_builder_restriction_feature = get_option('arm_is_fusion_builder_restriction_feature');
$oxygen_builder_restriction_feature = get_option('arm_is_oxygen_builder_restriction_feature');
$siteorigin_builder_restriction_feature = get_option('arm_is_siteorigin_builder_restriction_feature');
$bricks_builder_restriction_feature = get_option('arm_is_bricks_builder_restriction_feature');

$featureActiveIcon = MEMBERSHIP_IMAGES_URL . '/feature_active_icon.png';
if (is_rtl()) {
	$featureActiveIcon = MEMBERSHIP_IMAGES_URL . '/feature_active_icon_rtl.png';
}
?>
<?php
$hostname = $_SERVER["SERVER_NAME"]; //phpcs:ignore
global $arm_members_activity;
$setact = 0;
global $check_sorting;
$setact = $arm_members_activity->$check_sorting();
?>
<style>
    .purchased_info{ color:#7cba6c; font-weight:bold; font-size: 15px; }
	#license_success{ color:#8ccf7a !important; }
	.arperrmessage{color:red;}
    #arfactlicenseform { border-radius:0px; text-align:center; width:570px; min-height:350px; height:auto; left:35%; border:none; background:#ffffff !important; padding:30px 20px; }
    #wpcontent{ background: #EEF2F8; }
	#arfactlicenseform .form-table th{ text-align:right; }
	#arfactlicenseform .form-table td{ text-align:left; }
	#license_error{ color:red;}
	.arfnewmodalclose{ font-size: 15px; font-weight: bold; height: 19px; position: absolute; right: 3px; top:5px; width: 19px; cursor:pointer; color:#D1D6E5; }
	#licenseactivatedmessage { height:22px; color:#FFFFFF; font-size:17px; font-weight:bold; letter-spacing:0.5; margin-left:0px; display:block; border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px; -o-border-radius:3px; padding:7px 5px 5px 0px; font-family:'open_sansregular', Arial, Helvetica, Verdana, sans-serif; background-color:#8ccf7a; margin-top:15px !important; margin-bottom:10px !important; text-align:center; }
	.red_remove_license_btn { -moz-box-sizing: content-box; background: #e95a5a;  border:none; box-shadow: 0 4px 0 0 #d23939; color: #FFFFFF !important; cursor: pointer; font-size: 16px !important; font-style: normal; font-weight: bold; height: 30px; min-width: 90px; width: auto; outline: none; padding: 0px 10px; text-shadow: none; text-transform: none; vertical-align:middle; text-align:center; margin-bottom:15px; }
    .red_remove_license_btn:hover { background: #d23939; box-shadow: 0 4px 0 0 #b83131; }
    .newform_modal_title { font-size:25px; line-height:25px; margin-bottom: 10px; }
    .newmodal_field_title { font-size: 16px; line-height: 16px; margin-bottom: 10px; }
    .page_title.arm_new_addon_page_design{ font-size: 28px; line-height: 48px; text-align: center; padding: 24px 0; }
    .page_title.arm_new_addon_page_design.arm_new_addon_page_design_other_sec{ padding-top: 35px !important; }
    @media only screen and (max-width: 1024px) {
        .arm_feature_settings_wrapper{ margin: 0 15px; }
        .wrap.arm_page:not(.arm_manage_form_main_wrapper){ padding: 25px 15px; }
        .page_title.arm_new_addon_page_design.arm_new_addon_page_design_other_sec{ font-size: 25px; padding-top: 26px !important; padding-bottom: 0; }
        .page_title.arm_new_addon_page_design{ font-size: 25px; padding: 15px 0 10px 0; }
    }
    @media only screen and (max-width: 576px) {
        .arm_feature_settings_wrapper{ margin: 0 10px; }
        .page_title.arm_new_addon_page_design{ font-size: 22px; padding: 20px 0; }
        .page_title.arm_new_addon_page_design.arm_new_addon_page_design_other_sec{ padding-top: 20px !important; padding-bottom: 0; }
        .arm_feature_list .arm_feature_text { padding: 0 10px }
    }
</style>
<div class="wrap arm_page arm_feature_settings_main_wrapper">
    <?php
    if ($setact != 1) {
        $admin_css_url = admin_url('admin.php?page=arm_manage_license');
        ?>
        <div style="margin-top:20px;margin-bottom:20px;border-left: 4px solid #ffba00;box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.1);height:20px;width:99%;padding:10px 0px 10px 10px;background-color:#ffffff;color:#000000;font-size:16px;display:block;visibility:visible;text-align:left;" >ARMember License is not activated. Please activate license from <a href="<?php echo esc_url($admin_css_url); ?>">here</a></div>
    <?php } ?>
    <div class="content_wrapper arm_feature_settings_content" id="content_wrapper">
        <div class="page_title arm_new_addon_page_design"><?php esc_html_e('In-built Modules', 'ARMember'); ?></div>
        <div class="armclear"></div>
        <div class="arm_feature_settings_wrapper">            
            <div class="arm_feature_settings_container">
                <div class="arm_feature_list social_enable <?php echo ($social_feature == 1) ? 'active':'';?>">
                    <div class="arm_feature_icon"></div>
					<div class="arm_feature_active_icon"><div class="arm_check_mark"></div></div>
					<div class="arm_feature_content">
						<div class="arm_feature_title"><?php esc_html_e('Social Feature','ARMember'); ?></div>
						<div class="arm_feature_text"><?php esc_html_e("With this feature, enable social activities like Member Directory/Public Profile, Membership Cards and User Badges etc.", 'ARMember');?></div>
                        <?php if ($setact != 1) { ?>
						<div class="arm_feature_button_activate_wrapper <?php echo ($social_feature == 1) ? 'hidden_section':'';?>">
							<a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="social"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
						</div>
                        <?php } else { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($social_feature == 1) ? 'hidden_section':'';?>">
							<a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_settings_switch" data-feature_val="1" data-feature="social"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
						</div>
                        <?php } ?>
                        
						<div class="arm_feature_button_deactivate_wrapper <?php echo ($social_feature == 1) ? '':'hidden_section';?>">
							<a href="javascript:void(0)" class="arm_feature_deactivate_btn arm_feature_settings_switch" data-feature_val="0" data-feature="social"><?php esc_html_e('Deactivate','ARMember'); ?></a>
							<a href="<?php echo esc_url(admin_url('admin.php?page=' . $arm_slugs->profiles_directories));?>" class="arm_feature_configure_btn"><?php esc_html_e('Configure','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
						</div>
					</div>
                    <a class="arm_ref_info_links arm_feature_link" target="_blank" href="https://www.armemberplugin.com/documents/brief-of-social-features/"><?php esc_html_e('More Info', 'ARMember'); ?></a>
				</div>
                <!-- Start Pro-Rata Add-on -->
                <div class="arm_feature_list pro_ration_enable <?php echo ($pro_ration_feature == 1) ? 'active':'';?>">
					<div class="arm_feature_icon"></div>
                    <div class="arm_feature_active_icon"><div class="arm_check_mark"></div></div>
					<div class="arm_feature_content">
						<div class="arm_feature_title"><?php esc_html_e('Pro-Rata','ARMember'); ?></div>
						<div class="arm_feature_text"><?php esc_html_e("Allows member to purchase membership plan through Pro-Rata.", 'ARMember');?></div>
                        <?php if ($setact != 1) { ?>
						<div class="arm_feature_button_activate_wrapper <?php echo ($pro_ration_feature == 1) ? 'hidden_section':'';?>">
							<a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="pro_ration"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
						</div>
                        <?php } else { ?>
						<div class="arm_feature_button_activate_wrapper <?php echo ($pro_ration_feature == 1) ? 'hidden_section':'';?>">
							<a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_settings_switch" data-feature_val="1" data-feature="pro_ration"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
						</div>
                        <?php } ?>
						<div class="arm_feature_button_deactivate_wrapper <?php echo ($pro_ration_feature == 1) ? '':'hidden_section';?>">
							<a href="javascript:void(0)" class="arm_feature_deactivate_btn arm_feature_settings_switch" data-feature_val="0" data-feature="pro_ration"><?php esc_html_e('Deactivate','ARMember'); ?></a>
							<a href="<?php echo esc_url(admin_url('admin.php?page=' . $arm_slugs->general_settings));?>#ARMProRataConfig" class="arm_feature_configure_btn"><?php esc_html_e('Configure','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
						</div>
					</div>
                    <a class="arm_ref_info_links arm_feature_link arm_advanced_link" target="_blank" href="https://www.armemberplugin.com/documents/pro-rata/"><?php esc_html_e('More Info', 'ARMember'); ?></a>
                </div>
                <!-- End Pro-Rata Add-on -->
                <div class="arm_feature_list drip_content_enable <?php echo ($drip_content_feature == 1) ? 'active':'';?>">
					<div class="arm_feature_icon"></div>
                    <div class="arm_feature_active_icon"><div class="arm_check_mark"></div></div>
					<div class="arm_feature_content">
						<div class="arm_feature_title"><?php esc_html_e('Drip Content','ARMember'); ?></div>
						<div class="arm_feature_text"><?php esc_html_e("Publish your site content based on different time intervals by enabling this feature.", 'ARMember');?></div>
                        <?php if ($setact != 1) { ?>
						<div class="arm_feature_button_activate_wrapper <?php echo ($drip_content_feature == 1) ? 'hidden_section':'';?>">
							<a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="social"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
						</div>
                        <?php } else { ?>
						<div class="arm_feature_button_activate_wrapper <?php echo ($drip_content_feature == 1) ? 'hidden_section':'';?>">
							<a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_settings_switch" data-feature_val="1" data-feature="drip_content"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
						</div>
                        <?php } ?>
						<div class="arm_feature_button_deactivate_wrapper <?php echo ($drip_content_feature == 1) ? '':'hidden_section';?>">
							<a href="javascript:void(0)" class="arm_feature_deactivate_btn arm_feature_settings_switch" data-feature_val="0" data-feature="drip_content"><?php esc_html_e('Deactivate','ARMember'); ?></a>
							<a href="<?php echo esc_url(admin_url('admin.php?page=' . $arm_slugs->drip_rules));?>" class="arm_feature_configure_btn"><?php esc_html_e('Configure','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
						</div>
					</div>
                    <a class="arm_ref_info_links arm_feature_link" target="_blank" href="https://www.armemberplugin.com/documents/enable-drip-content-for-your-site/"><?php esc_html_e('More Info', 'ARMember'); ?></a>
                </div>
				<div class="arm_feature_list social_login_enable <?php echo ($social_login_feature == 1) ? 'active':'';?>">
					<div class="arm_feature_icon"></div>
                    <div class="arm_feature_active_icon"><div class="arm_check_mark"></div></div>
					<div class="arm_feature_content">
						<div class="arm_feature_title"><?php esc_html_e('Social Connect','ARMember'); ?></div>
						<div class="arm_feature_text"><?php esc_html_e("Allow users to sign up / login with their social accounts by enabling this feature.", 'ARMember');?></div>
                        <?php if ($setact != 1) { ?>
						<div class="arm_feature_button_activate_wrapper <?php echo ($social_login_feature == 1) ? 'hidden_section':'';?>">
							<a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="social"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
						</div>
                        <?php } else { ?>
						<div class="arm_feature_button_activate_wrapper <?php echo ($social_login_feature == 1) ? 'hidden_section':'';?>">
							<a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_settings_switch" data-feature_val="1" data-feature="social_login"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
						</div>
                        <?php } ?>
						<div class="arm_feature_button_deactivate_wrapper <?php echo ($social_login_feature == 1) ? '':'hidden_section';?>">
							<a href="javascript:void(0)" class="arm_feature_deactivate_btn arm_feature_settings_switch" data-feature_val="0" data-feature="social_login"><?php esc_html_e('Deactivate','ARMember'); ?></a>
							<a href="<?php echo esc_url(admin_url('admin.php?page=' . $arm_slugs->general_settings . '&action=social_options'));?>" class="arm_feature_configure_btn"><?php esc_html_e('Configure','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
						</div>
					</div>
                    <a class="arm_ref_info_links arm_feature_link arm_advanced_link" target="_blank" href="https://www.armemberplugin.com/documents/basic-information-for-social-login/"><?php esc_html_e('More Info', 'ARMember'); ?></a>
                </div>

                <!-- rpt_log new module -->
                <div class="arm_feature_list pay_per_post_enable <?php echo ($arm_pay_per_post == 1) ? 'active':'';?>">
                    <div class="arm_feature_icon"></div>
                    <div class="arm_feature_active_icon"><div class="arm_check_mark"></div></div>
                    <div class="arm_feature_content">
                        <div class="arm_feature_title"><?php esc_html_e('Pay Per Post','ARMember'); ?></div>
                        <div class="arm_feature_text"><?php esc_html_e("With this feature, you can sell post separately without creating plan(s).", 'ARMember');?></div>
                        <?php if ($setact != 1) { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($arm_pay_per_post == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="pay_per_post"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } else {  ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($arm_pay_per_post == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_settings_switch" data-feature_val="1" data-feature="pay_per_post"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } ?>
                        
                        <div class="arm_feature_button_deactivate_wrapper <?php echo ($arm_pay_per_post == 1) ? '':'hidden_section';?>">
                            <a href="javascript:void(0)" class="arm_feature_deactivate_btn arm_feature_settings_switch arm_no_config_feature_btn" data-feature_val="0" data-feature="pay_per_post"><?php esc_html_e('Deactivate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                    </div>
                    <a class="arm_ref_info_links arm_feature_link" target="_blank" href="https://www.armemberplugin.com/documents/pay-per-post/"><?php esc_html_e('More Info', 'ARMember'); ?></a>
                </div>
                <!-- rpt_log new module -->

                <div class="arm_feature_list coupon_enable <?php echo ($coupon_feature == 1) ? 'active':'';?>">
                    <div class="arm_feature_icon"></div>
                    <div class="arm_feature_active_icon"><div class="arm_check_mark"></div></div>
                    <div class="arm_feature_content">
                        <div class="arm_feature_title"><?php esc_html_e('Coupon','ARMember'); ?></div>
                        <div class="arm_feature_text"><?php esc_html_e("Let users get benefit of discounts coupons while making payment with your site.", 'ARMember');?></div>
                        <?php if ($setact != 1) { ?>
						<div class="arm_feature_button_activate_wrapper <?php echo ($coupon_feature == 1) ? 'hidden_section':'';?>">
							<a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="social"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
						</div>
                        <?php } else { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($coupon_feature == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_settings_switch" data-feature_val="1" data-feature="coupon"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } ?>
                        <div class="arm_feature_button_deactivate_wrapper <?php echo ($coupon_feature == 1) ? '':'hidden_section';?>">
                            <a href="javascript:void(0)" class="arm_feature_deactivate_btn arm_feature_settings_switch" data-feature_val="0" data-feature="coupon"><?php esc_html_e('Deactivate','ARMember'); ?></a>
                            <a href="<?php echo esc_url(admin_url('admin.php?page=' . $arm_slugs->coupon_management));?>" class="arm_feature_configure_btn"><?php esc_html_e('Configure','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                    </div>
                    <a class="arm_ref_info_links arm_feature_link arm_advanced_link" target="_blank" href="https://www.armemberplugin.com/documents/how-to-do-coupon-management/"><?php esc_html_e('More Info', 'ARMember'); ?></a>
                </div>
                <div class="arm_feature_list invoice_tax_enable <?php echo ($invoice_tax_feature == 1) ? 'active':'';?>">
                    <div class="arm_feature_icon"></div>
                    <div class="arm_feature_active_icon"><div class="arm_check_mark"></div></div>
                    <div class="arm_feature_content">
                        <div class="arm_feature_title"><?php esc_html_e('Invoice and Tax','ARMember'); ?></div>
                        <div class="arm_feature_text"><?php esc_html_e("Enable facility to send Invoice and apply Sales Tax on membership plans.", 'ARMember');?></div>
                        <?php if ($setact != 1) { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($invoice_tax_feature == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="social"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } else { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($invoice_tax_feature == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_settings_switch" data-feature_val="1" data-feature="invoice_tax"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } ?>
                        <div class="arm_feature_button_deactivate_wrapper <?php echo ($invoice_tax_feature == 1) ? '':'hidden_section';?>">
                            <a href="javascript:void(0)" class="arm_feature_deactivate_btn arm_feature_settings_switch" data-feature_val="0" data-feature="invoice_tax"><?php esc_html_e('Deactivate','ARMember'); ?></a>
                            <a href="<?php echo esc_url(admin_url('admin.php?page=' . $arm_slugs->general_settings));?>" class="arm_feature_configure_btn"><?php esc_html_e('Configure','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                    </div>
                    <a class="arm_ref_info_links arm_feature_link arm_advanced_link" target="_blank" href="https://www.armemberplugin.com/documents/invoice-and-tax"><?php esc_html_e('More Info', 'ARMember'); ?></a>
                </div>
                <!-- rpt_log new module -->
                <div class="arm_feature_list user_private_content_enable <?php echo ($user_private_content == 1) ? 'active':'';?>">
                    <div class="arm_feature_icon"></div>
                    <div class="arm_feature_active_icon"><div class="arm_check_mark"></div></div>
                    <div class="arm_feature_content">
                        <div class="arm_feature_title"><?php esc_html_e('User Private Content','ARMember'); ?></div>
                        <div class="arm_feature_text"><?php esc_html_e("With this feature, you can set different content for different user.", 'ARMember');?></div>
                        <?php if ($setact != 1) { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($user_private_content == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="user_private_content"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } else {  ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($user_private_content == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_settings_switch" data-feature_val="1" data-feature="user_private_content"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } ?>
                        
                        <div class="arm_feature_button_deactivate_wrapper <?php echo ($user_private_content == 1) ? '':'hidden_section';?>">
                            <a href="javascript:void(0)" class="arm_feature_deactivate_btn arm_feature_settings_switch" data-feature_val="0" data-feature="user_private_content"><?php esc_html_e('Deactivate','ARMember'); ?></a>
                            <a href="<?php echo esc_url(admin_url('admin.php?page=' . $arm_slugs->private_content));?>" class="arm_feature_configure_btn"><?php esc_html_e('Configure','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                    </div>
                    <a class="arm_ref_info_links arm_feature_link" target="_blank" href="https://www.armemberplugin.com/documents/user-private-content/"><?php esc_html_e('More Info', 'ARMember'); ?></a>
                </div>
                <!-- rpt_log new module -->
                <div class="arm_feature_list multiple_membership_enable <?php echo ($multiple_membership_feature == 1) ? 'active':'';?>">
                    <div class="arm_feature_icon"></div>
                    <div class="arm_feature_active_icon"><div class="arm_check_mark"></div></div>
                    <div class="arm_feature_content">
                        <div class="arm_feature_title"><?php esc_html_e('Multiple Membership/Plans','ARMember'); ?></div>
                        <div class="arm_feature_text"><?php esc_html_e("Allow members to subscribe multiple plans simultaneously.", 'ARMember');?></div>
                        <?php if ($setact != 1) { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($multiple_membership_feature == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="multiple_membership"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } else { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($multiple_membership_feature == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_settings_switch" data-feature_val="1" data-feature="multiple_membership"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } ?>
                        <div class="arm_feature_button_deactivate_wrapper <?php echo ($multiple_membership_feature == 1) ? '':'hidden_section';?>">
                            <a href="javascript:void(0)" class="arm_feature_deactivate_btn arm_no_config_feature_btn arm_feature_settings_switch" data-feature_val="0" data-feature="multiple_membership"><?php esc_html_e('Deactivate','ARMember'); ?></a>
                    
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                    </div>
                    <a class="arm_ref_info_links arm_feature_link arm_advanced_link" target="_blank" href="https://www.armemberplugin.com/documents/single-vs-multiple-membership/"><?php esc_html_e('More Info', 'ARMember'); ?></a>
                </div>
                <div class="arm_feature_list plan_limit_enable <?php echo ($plan_limit_feature == 1) ? 'active':'';?>">
                    <div class="arm_feature_icon"></div>
                    <div class="arm_feature_active_icon"><div class="arm_check_mark"></div></div>
                    <div class="arm_feature_content">
                        <div class="arm_feature_title"><?php esc_html_e('Membership Limit','ARMember'); ?></div>
                        <div class="arm_feature_text"><?php esc_html_e("With this feature, you can limit plan, Pay Per Post purchases for members.", 'ARMember');?></div>
                        <?php if ($setact != 1) { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($plan_limit_feature == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="plan_limit"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } else {  ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($plan_limit_feature == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_settings_switch" data-feature_val="1" data-feature="plan_limit"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } ?>
                        
                        <div class="arm_feature_button_deactivate_wrapper <?php echo ($plan_limit_feature == 1) ? '':'hidden_section';?>">
                            <a href="javascript:void(0)" class="arm_feature_deactivate_btn arm_feature_settings_switch arm_no_config_feature_btn" data-feature_val="0" data-feature="plan_limit"><?php esc_html_e('Deactivate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                    </div>
                    <a class="arm_ref_info_links arm_feature_link" target="_blank" href="https://www.armemberplugin.com/documents/paid-membership-plan-payment-process/#ARMMembershipLimit"><?php esc_html_e('More Info', 'ARMember'); ?></a>
                </div>
		
                <div class="arm_feature_list api_service_enable <?php echo ($arm_api_service_feature == 1) ? 'active' : '';?>">
                    <div class="arm_feature_icon"></div>
                    <div class="arm_feature_active_icon"><div class="arm_check_mark"></div></div>
                    <div class="arm_feature_content">
                        <div class="arm_feature_title"><?php esc_html_e('API Services', 'ARMember'); ?></div>
                        <div class="arm_feature_text"><?php esc_html_e("With this feature, you will able to use Membership API Services for your Application.", 'ARMember');?></div>
                        <?php if ($setact != 1) { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($arm_api_service_feature == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="api_service"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } else { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($arm_api_service_feature == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_settings_switch" data-feature_val="1" data-feature="api_service"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } ?>
                        <div class="arm_feature_button_deactivate_wrapper <?php echo ($arm_api_service_feature == 1) ? '':'hidden_section';?>">
                            <a href="javascript:void(0)" class="arm_feature_deactivate_btn arm_feature_settings_switch" data-feature_val="0" data-feature="api_service"><?php esc_html_e('Deactivate','ARMember'); ?></a>
                            <a href="<?php echo esc_url(admin_url('admin.php?page=' . $arm_slugs->general_settings));?>&action=api_service_feature" class="arm_feature_configure_btn"><?php esc_html_e('Configure','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                    </div>
                    <a class="arm_ref_info_links arm_feature_link arm_advanced_link" target="_blank" href="https://www.armemberplugin.com/documents/api-service-feature/"><?php esc_html_e('More Info', 'ARMember'); ?></a>
                </div>

                <div class="arm_feature_list buddypress_enable <?php echo ($buddypress_feature == 1) ? 'active':'';?>">
                    <div class="arm_feature_icon"></div>
                    <div class="arm_feature_active_icon"><div class="arm_check_mark"></div></div>
                    <div class="arm_feature_content">
                        <div class="arm_feature_title"><?php esc_html_e('Buddypress/Buddyboss Integration','ARMember'); ?></div>
                        <div class="arm_feature_text"><?php esc_html_e("Integrate BuddyPress or BuddyBoss with ARMember.", 'ARMember');?></div>
                        <?php if ($setact != 1) { ?>
						<div class="arm_feature_button_activate_wrapper <?php echo ($buddypress_feature == 1) ? 'hidden_section':'';?>">
							<a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="social"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
						</div>
                        <?php } else { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($buddypress_feature == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_settings_switch" data-feature_val="1" data-feature="buddypress"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } ?>
                        <div class="arm_feature_button_deactivate_wrapper <?php echo ($buddypress_feature == 1) ? '':'hidden_section';?>">
                            <a href="javascript:void(0)" class="arm_feature_deactivate_btn arm_feature_settings_switch" data-feature_val="0" data-feature="buddypress"><?php esc_html_e('Deactivate','ARMember'); ?></a>
                            <a href="<?php echo esc_url(admin_url('admin.php?page=' . $arm_slugs->general_settings . '&action=buddypress_options'));?>" class="arm_feature_configure_btn"><?php esc_html_e('Configure','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                    </div>
                    <a class="arm_ref_info_links arm_feature_link arm_advanced_link" target="_blank" href="https://www.armemberplugin.com/documents/buddypress-support/"><?php esc_html_e('More Info', 'ARMember'); ?></a>
                </div>
                
                <div class="arm_feature_list woocommerce_enable <?php echo ($woocommerce_feature == 1) ? 'active':'';?>">
                    <div class="arm_feature_icon"></div>
                    <div class="arm_feature_active_icon"><div class="arm_check_mark"></div></div>
                    <div class="arm_feature_content">
                        <div class="arm_feature_title"><?php esc_html_e('Woocommerce Integration','ARMember'); ?></div>
                        <div class="arm_feature_text" style=" min-height: 0;"><?php esc_html_e("Integrate Woocommerce with ARMember.", 'ARMember');?></div>
                        <div class="arm_feature_text arm_woocommerce_feature_version_required_notice" ><?php esc_html_e('Min Required Woocommerce Ver.: 3.0.2', 'ARMember');?></div>
                        <?php if ($setact != 1) { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($woocommerce_feature == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="woocommerce"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } else { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($woocommerce_feature == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_settings_switch" data-feature_val="1" data-feature="woocommerce"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } ?>
                        <div class="arm_feature_button_deactivate_wrapper <?php echo ($woocommerce_feature == 1) ? '':'hidden_section';?>">
                            <a href="javascript:void(0)" class="arm_feature_deactivate_btn arm_feature_settings_switch arm_no_config_feature_btn" data-feature_val="0" data-feature="woocommerce"><?php esc_html_e('Deactivate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                    </div>
                    <a class="arm_ref_info_links arm_feature_link arm_advanced_link" target="_blank" href="https://www.armemberplugin.com/documents/woocommerce-support/"><?php esc_html_e('More Info', 'ARMember'); ?></a>
                </div>


                <div class="arm_feature_list mycred_enable <?php echo ($arm_admin_mycred_feature->ismyCREDFeature == true) ? 'active':'';?>">
                    <div class="arm_feature_icon"></div>
                    <div class="arm_feature_active_icon"><div class="arm_check_mark"></div></div>
                    <div class="arm_feature_content">
                        <div class="arm_feature_title"><?php esc_html_e('myCRED Integration','ARMember'); ?></div>
                        <div class="arm_feature_text"><?php esc_html_e("Integrate myCRED adaptive points management system with ARMember.", 'ARMember');?></div>
                        <?php if ($setact != 1) { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($arm_admin_mycred_feature->ismyCREDFeature == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="social"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } else { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($arm_admin_mycred_feature->ismyCREDFeature == true) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_settings_switch" data-feature_val="1" data-feature="mycred"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } ?>
                        <div class="arm_feature_button_deactivate_wrapper <?php echo ($arm_admin_mycred_feature->ismyCREDFeature == true) ? '':'hidden_section';?>">
                            <a href="javascript:void(0)" class="arm_feature_deactivate_btn arm_feature_settings_switch" data-feature_val="0" data-feature="mycred"><?php esc_html_e('Deactivate','ARMember'); ?></a>
                            <a href="<?php echo esc_url(admin_url('admin.php?page=mycred-hooks'));?>" class="arm_feature_configure_btn"><?php esc_html_e('Configure','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                    </div>
                    <a class="arm_ref_info_links arm_feature_link arm_advanced_link" target="_blank" href="https://www.armemberplugin.com/documents/mycred-integration/"><?php esc_html_e('More Info', 'ARMember'); ?></a>
                </div>
                
                <!-- Start Armember Gutenberg Block Integration -->
                <div class="arm_feature_list gutenberg_block_restriction_enable <?php echo ($gutenberg_block_restriction_feature == 1) ? 'active':'';?>">
                    <div class="arm_feature_icon"></div>
                    <div class="arm_feature_active_icon"><div class="arm_check_mark"></div></div>
                    <div class="arm_feature_content">
                        <div class="arm_feature_title"><?php esc_html_e('Gutenberg Block Integration','ARMember'); ?></div>
                        <div class="arm_feature_text"><?php esc_html_e("Allows facility to set the Access for Gutenberg Blocks per Membership Plan or Logged in member.", 'ARMember');?></div>
                        <?php if ($setact != 1) { ?>
                            <div class="arm_feature_button_activate_wrapper <?php echo ($gutenberg_block_restriction_feature == 1) ? 'hidden_section':'';?>">
                                <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="gutenberg_block_restriction"><?php esc_html_e('Activate','ARMember'); ?></a>
                                <span class="arm_addon_loader">
                                    <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } else { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($gutenberg_block_restriction_feature == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_settings_switch" data-feature_val="1" data-feature="gutenberg_block_restriction"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } ?>
                        <div class="arm_feature_button_deactivate_wrapper <?php echo ($gutenberg_block_restriction_feature == 1) ? '':'hidden_section';?>">
                            <a href="javascript:void(0)" class="arm_feature_deactivate_btn arm_no_config_feature_btn arm_feature_settings_switch" data-feature_val="0" data-feature="gutenberg_block_restriction"><?php esc_html_e('Deactivate','ARMember'); ?></a>
                            
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                    </div>
                    <a class="arm_ref_info_links arm_feature_link arm_advanced_link" target="_blank" href="https://www.armemberplugin.com/documents/gutenberg-block-support/"><?php esc_html_e('More Info', 'ARMember'); ?></a>
                </div>
                <!-- End Armember Gutenberg Block Integration -->

                <!-- Start Armember Beaver Builder Integration -->
                <div class="arm_feature_list beaver_builder_restriction_enable <?php echo ($beaver_builder_restriction_feature == 1) ? 'active':'';?>">
                    <div class="arm_feature_icon"></div>
                    <div class="arm_feature_active_icon"><div class="arm_check_mark"></div></div>
                    <div class="arm_feature_content">
                        <div class="arm_feature_title"><?php esc_html_e('Beaver Builder Integration','ARMember'); ?></div>
                        <div class="arm_feature_text"><?php esc_html_e("Allows Beaver Builder widgets to restrict based on Membership Plan.", 'ARMember');?></div>
                        <?php if ($setact != 1) { ?>
                            <div class="arm_feature_button_activate_wrapper <?php echo ($beaver_builder_restriction_feature == 1) ? 'hidden_section':'';?>">
                                <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="beaver_builder_restriction"><?php esc_html_e('Activate','ARMember'); ?></a>
                                <span class="arm_addon_loader">
                                    <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } else { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($beaver_builder_restriction_feature == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_settings_switch" data-feature_val="1" data-feature="beaver_builder_restriction"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } ?>
                        <div class="arm_feature_button_deactivate_wrapper <?php echo ($beaver_builder_restriction_feature == 1) ? '':'hidden_section';?>">
                            <a href="javascript:void(0)" class="arm_feature_deactivate_btn arm_no_config_feature_btn arm_feature_settings_switch" data-feature_val="0" data-feature="beaver_builder_restriction"><?php esc_html_e('Deactivate','ARMember'); ?></a>
                            
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                    </div>
                    <a class="arm_ref_info_links arm_feature_link arm_advanced_link" target="_blank" href="https://www.armemberplugin.com/documents/beaver-builder-support/"><?php esc_html_e('More Info', 'ARMember'); ?></a>
                </div>
                <!-- End Armember Beaver Builder Integration -->

                <!-- Start Armember DIVI Builder Integration -->
                <div class="arm_feature_list divi_builder_restriction_enable <?php echo ($divi_builder_restriction_feature == 1) ? 'active':'';?>">
                    <div class="arm_feature_icon"></div>
                    <div class="arm_feature_active_icon"><div class="arm_check_mark"></div></div>
                    <div class="arm_feature_content">
                        <div class="arm_feature_title"><?php esc_html_e('Divi Builder Integration','ARMember'); ?></div>
                        <div class="arm_feature_text"><?php esc_html_e("Allows facility to set the access for Divi Builder content Like Section and Row per Membership Plan.", 'ARMember');?></div>
                        <?php if ($setact != 1) { ?>
                            <div class="arm_feature_button_activate_wrapper <?php echo ($divi_builder_restriction_feature == 1) ? 'hidden_section':'';?>">
                                <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="divi_builder_restriction"><?php esc_html_e('Activate','ARMember'); ?></a>
                                <span class="arm_addon_loader">
                                    <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } else { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($divi_builder_restriction_feature == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_settings_switch" data-feature_val="1" data-feature="divi_builder_restriction"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } ?>
                        <div class="arm_feature_button_deactivate_wrapper <?php echo ($divi_builder_restriction_feature == 1) ? '':'hidden_section';?>">
                            <a href="javascript:void(0)" class="arm_feature_deactivate_btn arm_no_config_feature_btn arm_feature_settings_switch" data-feature_val="0" data-feature="divi_builder_restriction"><?php esc_html_e('Deactivate','ARMember'); ?></a>
                            
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                    </div>
                    <a class="arm_ref_info_links arm_feature_link arm_advanced_link" target="_blank" href="https://www.armemberplugin.com/documents/divi-builder-support/"><?php esc_html_e('More Info', 'ARMember'); ?></a>
                </div>
                <!-- End Armember DIVI Builder Integration -->

                <!-- Start Armember WPBakery page Builder Integration -->
                <div class="arm_feature_list wpbakery_page_builder_restriction_enable <?php echo ($wpbakery_page_builder_restriction_feature == 1) ? 'active':'';?>">
                    <div class="arm_feature_icon"></div>
                    <div class="arm_feature_active_icon"><div class="arm_check_mark"></div></div>
                    <div class="arm_feature_content">
                        <div class="arm_feature_title"><?php esc_html_e('WPBakery Page Builder Integration','ARMember'); ?></div>
                        <div class="arm_feature_text"><?php esc_html_e("Allows to set restrict content on WPBakery Elements per Membership Plan.", 'ARMember');?></div>
                        <?php if ($setact != 1) { ?>
                            <div class="arm_feature_button_activate_wrapper <?php echo ($wpbakery_page_builder_restriction_feature == 1) ? 'hidden_section':'';?>">
                                <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="wpbakery_page_builder_restriction"><?php esc_html_e('Activate','ARMember'); ?></a>
                                <span class="arm_addon_loader">
                                    <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } else { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($wpbakery_page_builder_restriction_feature == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_settings_switch" data-feature_val="1" data-feature="wpbakery_page_builder_restriction"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } ?>
                        <div class="arm_feature_button_deactivate_wrapper <?php echo ($wpbakery_page_builder_restriction_feature == 1) ? '':'hidden_section';?>">
                            <a href="javascript:void(0)" class="arm_feature_deactivate_btn arm_no_config_feature_btn arm_feature_settings_switch" data-feature_val="0" data-feature="wpbakery_page_builder_restriction"><?php esc_html_e('Deactivate','ARMember'); ?></a>
                            
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                    </div>
                    <a class="arm_ref_info_links arm_feature_link arm_advanced_link" target="_blank" href="https://www.armemberplugin.com/documents/visual-composer-support/#ARMWPBakerySupport"><?php esc_html_e('More Info', 'ARMember'); ?></a>
                </div>
                <!-- End Armember WPBakery page Builder Integration -->
                
                <!-- Start Armember Fusion Builder Integration -->
                <div class="arm_feature_list fusion_builder_restriction_enable <?php echo ($fusion_builder_restriction_feature == 1) ? 'active':'';?>">
                    <div class="arm_feature_icon"></div>
                    <div class="arm_feature_active_icon"><div class="arm_check_mark"></div></div>
                    <div class="arm_feature_content">
                        <div class="arm_feature_title"><?php esc_html_e('Fusion Builder Integration','ARMember'); ?></div>
                        <div class="arm_feature_text"><?php esc_html_e("Allows to set restrict content on Fusion Builder Containers & Columns per Membership Plan.", 'ARMember');?></div>
                        <?php if ($setact != 1) { ?>
                            <div class="arm_feature_button_activate_wrapper <?php echo ($fusion_builder_restriction_feature == 1) ? 'hidden_section':'';?>">
                                <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="fusion_builder_restriction"><?php esc_html_e('Activate','ARMember'); ?></a>
                                <span class="arm_addon_loader">
                                    <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } else { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($fusion_builder_restriction_feature == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_settings_switch" data-feature_val="1" data-feature="fusion_builder_restriction"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } ?>
                        <div class="arm_feature_button_deactivate_wrapper <?php echo ($fusion_builder_restriction_feature == 1) ? '':'hidden_section';?>">
                            <a href="javascript:void(0)" class="arm_feature_deactivate_btn arm_no_config_feature_btn arm_feature_settings_switch" data-feature_val="0" data-feature="fusion_builder_restriction"><?php esc_html_e('Deactivate','ARMember'); ?></a>
                            
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                    </div>
                    <a class="arm_ref_info_links arm_feature_link arm_advanced_link" target="_blank" href="https://www.armemberplugin.com/documents/fusion-builder-support/"><?php esc_html_e('More Info', 'ARMember'); ?></a>
                </div>
                <!-- End Armember Fusion Builder Integration -->
                
                <!-- Start Armember Oxygen Builder Integration -->
                <div class="arm_feature_list oxygen_builder_restriction_enable <?php echo ($oxygen_builder_restriction_feature == 1) ? 'active':'';?>">
                    <div class="arm_feature_icon"></div>
                    <div class="arm_feature_active_icon"><div class="arm_check_mark"></div></div>
                    <div class="arm_feature_content">
                        <div class="arm_feature_title"><?php esc_html_e('Oxygen Builder Integration','ARMember'); ?></div>
                        <div class="arm_feature_text"><?php esc_html_e("Allows to set restrict content on Oxygen Builder Container, Section, Column and Components per Membership Plan.", 'ARMember');?></div>
                        <?php if ($setact != 1) { ?>
                            <div class="arm_feature_button_activate_wrapper <?php echo ($oxygen_builder_restriction_feature == 1) ? 'hidden_section':'';?>">
                                <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="oxygen_builder_restriction"><?php esc_html_e('Activate','ARMember'); ?></a>
                                <span class="arm_addon_loader">
                                    <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } else { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($oxygen_builder_restriction_feature == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_settings_switch" data-feature_val="1" data-feature="oxygen_builder_restriction"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } ?>
                        <div class="arm_feature_button_deactivate_wrapper <?php echo ($oxygen_builder_restriction_feature == 1) ? '':'hidden_section';?>">
                            <a href="javascript:void(0)" class="arm_feature_deactivate_btn arm_no_config_feature_btn arm_feature_settings_switch" data-feature_val="0" data-feature="oxygen_builder_restriction"><?php esc_html_e('Deactivate','ARMember'); ?></a>
                            
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                    </div>
                    <a class="arm_ref_info_links arm_feature_link arm_advanced_link" target="_blank" href="https://www.armemberplugin.com/documents/oxygen-builder-support/"><?php esc_html_e('More Info', 'ARMember'); ?></a>
                </div>
                <!-- End Armember Oxygen Builder Integration -->

                <!-- Start Armember Sietorigin Builder Integration -->
                <div class="arm_feature_list siteorigin_builder_restriction_enable <?php echo ($siteorigin_builder_restriction_feature == 1) ? 'active':'';?>">
                    <div class="arm_feature_icon"></div>
                    <div class="arm_feature_active_icon"><div class="arm_check_mark"></div></div>
                    <div class="arm_feature_content">
                        <div class="arm_feature_title"><?php esc_html_e('SiteOrigin Builder Integration','ARMember'); ?></div>
                        <div class="arm_feature_text"><?php esc_html_e("Allows to set restrict content on SiteOrigin Builder Row and Column per Membership Plan.", 'ARMember');?></div>
                        <?php if ($setact != 1) { ?>
                            <div class="arm_feature_button_activate_wrapper <?php echo ($siteorigin_builder_restriction_feature == 1) ? 'hidden_section':'';?>">
                                <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="siteorigin_builder_restriction"><?php esc_html_e('Activate','ARMember'); ?></a>
                                <span class="arm_addon_loader">
                                    <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } else { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($siteorigin_builder_restriction_feature == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_settings_switch" data-feature_val="1" data-feature="siteorigin_builder_restriction"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } ?>
                        <div class="arm_feature_button_deactivate_wrapper <?php echo ($siteorigin_builder_restriction_feature == 1) ? '':'hidden_section';?>">
                            <a href="javascript:void(0)" class="arm_feature_deactivate_btn arm_no_config_feature_btn arm_feature_settings_switch" data-feature_val="0" data-feature="siteorigin_builder_restriction"><?php esc_html_e('Deactivate','ARMember'); ?></a>
                            
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                    </div>
                    <a class="arm_ref_info_links arm_feature_link arm_advanced_link" target="_blank" href="https://www.armemberplugin.com/documents/siteorigin-builder-support/"><?php esc_html_e('More Info', 'ARMember'); ?></a>
                </div>
                <!-- End Armember Sietorigin Builder Integration -->
                
                <!-- Start Armember Bricks Builder Integration -->
                <div class="arm_feature_list bricks_builder_restriction_enable <?php echo ($bricks_builder_restriction_feature == 1) ? 'active':'';?>">
                    <div class="arm_feature_icon"></div>
                    <div class="arm_feature_active_icon"><div class="arm_check_mark"></div></div>
                    <div class="arm_feature_content">
                        <div class="arm_feature_title"><?php esc_html_e('Bricks Builder Integration','ARMember'); ?></div>
                        <div class="arm_feature_text"><?php esc_html_e("Allows to set restrict content on Bricks Builder Elements per Membership Plan.", 'ARMember');?></div>
                        <?php if ($setact != 1) { ?>
                            <div class="arm_feature_button_activate_wrapper <?php echo ($bricks_builder_restriction_feature == 1) ? 'hidden_section':'';?>">
                                <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="bricks_builder_restriction"><?php esc_html_e('Activate','ARMember'); ?></a>
                                <span class="arm_addon_loader">
                                    <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } else { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($bricks_builder_restriction_feature == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_settings_switch" data-feature_val="1" data-feature="bricks_builder_restriction"><?php esc_html_e('Activate','ARMember'); ?></a>
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                        <?php } ?>
                        <div class="arm_feature_button_deactivate_wrapper <?php echo ($bricks_builder_restriction_feature == 1) ? '':'hidden_section';?>">
                            <a href="javascript:void(0)" class="arm_feature_deactivate_btn arm_no_config_feature_btn arm_feature_settings_switch" data-feature_val="0" data-feature="bricks_builder_restriction"><?php esc_html_e('Deactivate','ARMember'); ?></a>
                            
                            <span class="arm_addon_loader">
                                <svg class="arm_circular" viewBox="0 0 60 60">
                                    <circle class="path" cx="25px" cy="23px" r="18" fill="none" stroke-width="4" stroke-miterlimit="7"></circle>
                                </svg>
                            </span>
                        </div>
                    </div>
                    <a class="arm_ref_info_links arm_feature_link arm_advanced_link" target="_blank" href="https://www.armemberplugin.com/documents/bricks-builder-support/"><?php esc_html_e('More Info', 'ARMember'); ?></a>
                </div>
                <!-- End Armember Bricks Builder Integration -->
                
                
                <?php echo do_action('arm_add_new_custom_add_on'); //phpcs:ignore?>
            </div>
            
				<?php
				global $arm_social_feature;
				global $arm_version;
				$addon_resp = "";
				$addon_resp = $arm_social_feature->addons_page();
				$plugins = get_plugins();
				$installed_plugins = array();
				foreach ($plugins as $key => $plugin) {
					$is_active = is_plugin_active($key);
					$installed_plugin = array("plugin" => $key, "name" => $plugin["Name"], "is_active" => $is_active);
					$installed_plugin["activation_url"] = $is_active ? "" : wp_nonce_url("plugins.php?action=activate&plugin={$key}", "activate-plugin_{$key}");
					$installed_plugin["deactivation_url"] = !$is_active ? "" : wp_nonce_url("plugins.php?action=deactivate&plugin={$key}", "deactivate-plugin_{$key}");

					$installed_plugins[] = $installed_plugin;
				}


		if ($addon_resp != "") {
		    $resp = explode("|^^|", $addon_resp);

		    if ($resp[0] == 1) {
			$myplugarr = array();
			$myplugarr = unserialize(base64_decode($resp[1]));


			$is_active = 0;
			if (is_array($myplugarr) && count($myplugarr) > 0) {
			    ?><?php
			    foreach ($myplugarr as $key => $plug_1) {
                    if($key == 'feature' ) {
                    ?>
                        <div class="page_title arm_new_addon_page_design arm_new_addon_page_design_other_sec"><?php esc_html_e('Additional Functionality Addon', 'ARMember'); ?></div>
                    <?php } 
                    if($key == 'payment_gateways' ) { ?>
                        <div class="page_title arm_new_addon_page_design arm_new_addon_page_design_other_sec"><?php esc_html_e('Payment Gateways Addons', 'ARMember'); ?></div>
                    <?php }  
                    if($key == 'integrations' ) { ?>
                        <div class="page_title arm_new_addon_page_design arm_new_addon_page_design_other_sec"><?php esc_html_e('Third-Party Integrations', 'ARMember'); ?></div>
                    <?php }  ?>
                    <div class="arm_feature_settings_container arm_margin_top_30 arm_margin_bottom_25">
                    <?php
                    foreach ($plug_1 as $key_1 => $plug) {
                        
                    $is_active_plugin = is_plugin_active($plug['plugin_installer']);
                    
                    $is_config = ( isset( $plug['display_config'] ) && 'yes' == $plug['display_config'] ) ? true : false;
                    $config_url = isset( $plug['config_args'] ) ? admin_url( $plug['config_args'] ) : '';
                    ?>
                    <div class="arm_feature_list <?php echo esc_attr($plug['short_name']); ?>_enable <?php echo ($is_active_plugin == 1) ? 'active' : ''; ?>">
                        <div class="arm_feature_icon" style="background-image:url(<?php echo esc_attr($plug['icon']); ?>);"></div>
                        <div class="arm_feature_active_icon"><div class="arm_check_mark"></div></div>
                        <div class="arm_feature_content">
                        <div class="arm_feature_title"><?php echo esc_html($plug['full_name']); ?></div>
                        <div class="arm_feature_text"><?php echo esc_html($plug['description']); ?></div>
                        
                        <?php if ($setact != 1) { ?>
                            <div class="arm_feature_button_activate_wrapper ">
                                <a href="javascript:void(0)" style="padding:0 15px !important;" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="<?php echo esc_attr($plug['short_name']); ?>"><?php esc_html_e('Activate License', 'ARMember'); ?></a>
                                <img src="<?php echo MEMBERSHIP_IMAGES_URL . '/arm_loader.gif' //phpcs:ignore?>" class="arm_addon_loader_img" width="24" height="24" />
                            </div>
                            <?php
                        } else {
                    ?>
                            <div class="arm_feature_button_activate_wrapper ">
                                <?php echo $arm_social_feature->CheckpluginStatus($installed_plugins, $plug['plugin_installer'], 'plugin', $plug['short_name'], $plug['plugin_type'], $plug['install_url'], $plug['armember_version'], $arm_version, $is_config, $config_url);  //phpcs:ignore?>
                                <img src="<?php echo MEMBERSHIP_IMAGES_URL . '/arm_loader.gif' //phpcs:ignore?>" class="arm_addon_loader_img" width="24" height="24" />
                            </div>
                        <?php } ?>
                        </div>
                        <?php if(!empty($plug['armember_version']) && $plug['armember_version']>$arm_version) { ?>
                                <div class="arm_feature_text arm_feature_vesrion_compatiblity arm_color_red arm_font_size_15" style="font-weight: bold;"><?php esc_html_e('Min. Required V', 'ARMember'); echo esc_html($plug['armember_version'])?></div>
                        <?php } ?>
                        <a class="arm_ref_info_links arm_feature_link arm_advanced_link" target="_blank" href="<?php echo esc_url($plug['detail_url']); ?>"><?php esc_html_e('More Info', 'ARMember'); ?></a>
                    </div>
                    <?php
                    }

			    }
            ?>
                </div>
            <?php
			}
		    }
			else if(!empty($resp[1])) {
				echo $resp[1]; //phpcs:ignore
			}
		}
		?>
	    
        </div>
        <?php $wpnonce = wp_create_nonce( 'arm_wp_nonce' );?>
        <input type="hidden" name="arm_wp_nonce" value="<?php echo esc_attr($wpnonce);?>"/>
        <div class="armclear"></div>
    </div>
</div>

<?php
$addon_content = '<span class="arm_confirm_text">'.esc_html__("You need to have ARMember version 1.6 OR higher to install this addon.",'ARMember' ).'</span>';
		$addon_content .= '<input type="hidden" value="false" id="bulk_delete_flag"/>';
		$addon_content_popup_arg = array(
			'id' => 'addon_message',
			'class' => 'adddon_message',
                        'title' => esc_html__('Confirmation','ARMember'),
			'content' => $addon_content,
			'button_id' => 'addon_ok_btn',
			'button_onclick' => "addon_message();",
		);
		echo $arm_global_settings->arm_get_bpopup_html($addon_content_popup_arg); //phpcs:ignore?>


<div id="arfactnotcompatible" style="display:none; background:white; padding:15px; border-radius:3px; width:400px; height:100px;">
		
		<div class="arfactnotcompatiblemodalclose" style="float:right;text-align:right;cursor:pointer; position:absolute;right:10px; " onclick="javascript:return false;"><img src="<?php echo MEMBERSHIP_IMAGES_URL . '/close-button.png'; //phpcs:ignore?>" align="absmiddle" /></div>
        
       <table class="form-table">
            <tr class="form-field">
                <th class="arm-form-table-label arm_font_size_16" >You need to have ARMember version 1.6 OR higher to install this addon.</th>
            </tr>				
		</table>
</div>
<div id="arfactlicenseform" style="display:none;">
		
		<div class="arfnewactmodalclose" style="float:right;text-align:right;cursor:pointer;" onclick="javascript:return false;"><img src="<?php echo MEMBERSHIP_IMAGES_URL . '/close-button.png'; //phpcs:ignore?>" align="absmiddle" /></div>
        <div class="newform_modal_title_container">
        	<div class="newform_modal_title">&nbsp;Product License</div>
    	</div>
    <?php 
        global $armember_check_plugin_copy, $ARMember;
        if( empty( $armember_check_plugin_copy ) )
        {
    ?>
       <table class="form-table">
            <tr class="form-field">
                <th class="arm-form-table-label"><?php esc_html_e('Customer Name', 'ARMember'); ?></th>
                <td class="arm-form-table-content">
                    <input type="text" name="li_customer_name" id="li_customer_name" value="" autocomplete="off" />
                    <div class="arperrmessage" id="li_customer_name_error" style="display:none;"><?php esc_html_e('This field cannot be blank.', 'ARMember'); ?></div>         
                </td>
            </tr>
            <tr class="form-field">
                <th class="arm-form-table-label"><?php esc_html_e('Customer Email', 'ARMember'); ?></th>
                <td class="arm-form-table-content">
                    <input type="text" name="li_customer_email" id="li_customer_email" value="" autocomplete="off" />
                </td>
            </tr>
            <tr class="form-field">
                <th class="arm-form-table-label"><?php esc_html_e('Purchase Code', 'ARMember'); ?></th>
                <td class="arm-form-table-content">
                    <input type="text" name="li_license_key" id="li_license_key" value="" autocomplete="off" />
                    <div class="arperrmessage" id="li_license_key_error" style="display:none;"><?php esc_html_e('This field cannot be blank.', 'ARMember'); ?></div>        
                </td>
            </tr>
            <tr class="form-field">
                <th class="arm-form-table-label"><?php esc_html_e('Domain Name', 'ARMember'); ?></th>
                <td class="arm-form-table-content">
                    <label class="lblsubtitle"><?php echo esc_html($hostname); ?></label>
                    <input type="hidden" name="li_domain_name" id="li_domain_name" value="<?php echo esc_attr($hostname); ?>" autocomplete="off" />        
                </td>
            </tr>
            <input type="hidden" name="receive_updates" id="receive_updates" value="0" autocomplete="off" />
            <tr class="form-field">
                <th class="arm-form-table-label">&nbsp;</th>
                <td class="arm-form-table-content">
                    <span id="license_link"><button type="button" id="verify-purchase-code-addon" name="continue" style="width:150px; cursor:pointer; background-color:#53ba73; border:0px; color:#FFFFFF; height:40px; border-radius:3px;" class="greensavebtn"><?php esc_html_e('Activate', 'ARMember'); ?></button></span>
                    <span id="license_loader" style="display:none;position:absolute;margin-top:12px; padding-left:10px;"><img src="<?php echo MEMBERSHIP_IMAGES_URL . '/loading_activation.gif'; //phpcs:ignore?>" height="15" /></span> 
                    <span id="license_error" style="display:none;position:absolute;margin-top:12px; padding-left:10px;">&nbsp;</span>
                    <span id="license_success" style="display:none;position:absolute;margin-top:12px;"><?php esc_html_e('License Activated Successfully.', 'ARMember'); ?></span>
                    <input type="hidden" name="ajaxurl" id="ajaxurl" value="<?php echo esc_url(admin_url('admin-ajax.php')); ?>"  />        
                </td>
            </tr>
            
        </table>
    <?php
        }
        else
        {   $arm_pkg_armember_content = "";
            $form_flag = 1;
            echo $ARMember->arm_armember_pkg_content_external($arm_pkg_armember_content, $form_flag ); //phpcs:ignore
        }
    ?>
</div> 
<script type="text/javascript">
    var ADDON_NOT_COMPATIBLE_MESSAGE = "<?php esc_html_e('This Addon is not compatible with current ARMember version. Please update ARMember to latest version.','ARMember'); ?>";
    <?php 
        if(!empty($_REQUEST['arm_activate_social_feature']))
        {
    ?>
            armToast("<?php esc_html_e('Please activate the \"Social Feature\" module to make this feature work.','ARMember'); ?>", 'error', 5000, false);
    <?php 
        }
        else if(!empty($_REQUEST['arm_activate_drip_feature'])) 
        {
    ?>
            armToast("<?php esc_html_e('Please activate the \"Drip Content\" module to make this feature work.','ARMember'); ?>", 'error', 5000, false);
    <?php 
        }
        else if(!empty($_REQUEST['arm_activate_private_content_feature']))
        {
    ?>
            armToast("<?php esc_html_e('Please activate the \"User Private Content\" module to make this feature work.','ARMember'); ?>", 'error', 5000, false);
    <?php
        }
        else if(!empty($_REQUEST['arm_activate_coupon_feature']))
        {
    ?>
            armToast("<?php esc_html_e('Please activate the \"Coupon\" module to make this feature work.','ARMember'); ?>", 'error', 5000, false);
    <?php
        }
        else if(!empty($_REQUEST['arm_activate_pay_per_pst_feature']))
        {
    ?>
            armToast("<?php esc_html_e('Please activate the \"Pay Per Post\" module to make this feature work.','ARMember'); ?>", 'error', 5000, false);
    <?php
        }
    ?>
</script>
    
<?php
$_SESSION['arm_member_addon'] = $myplugarr;